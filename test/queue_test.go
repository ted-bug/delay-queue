package test

import (
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"testing"
	"time"

	delayqueue "gitee.com/ted-bug/delay-queue"
)

func TestQueue(t *testing.T) {
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, syscall.SIGINT, syscall.SIGTERM)
	doTime := time.Now().Add(10 * time.Second).Unix()
	queue := delayqueue.New()
	jobData := delayqueue.JobData{
		Id:   "",
		Data: "hello world",
		Handler: func(id, data string) bool {
			fmt.Println(id, data, " world")
			return true
		},
	}
	queue.Run()
	for i := 0; i < 5; i++ {
		jobData.Id = strconv.Itoa(i)
		queue.AddJob(doTime+int64(i*10), jobData)
	}
	<-exit
	fmt.Println("test quit...")
}
