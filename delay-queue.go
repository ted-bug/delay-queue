package delayqueue

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// 任务单元
type JobData struct {
	Id      string
	Data    string
	Handler func(string, string) bool
}

// 基础配置
type DelayConf struct {
	WorkNums int // 消费goroutine数
}

// 延迟队列
type DelayQueue struct {
	delayQueue []sync.Map // 延时队列: 时间戳（秒）为键，值为同一时间启动消费的string(json格式)
	conf       DelayConf  // 配置
}

// 创建一个实例
func New() (queue DelayQueue) {
	conf := DefalutConf()
	delayQueue := make([]sync.Map, conf.WorkNums)
	queue = DelayQueue{
		delayQueue: delayQueue,
		conf:       conf,
	}
	return
}

// 默认配置
func DefalutConf() (conf DelayConf) {
	conf = DelayConf{
		WorkNums: 4,
	}
	return
}

// 创建一个实例，自定义配置
func NewWithConf(conf DelayConf) (queue DelayQueue) {
	delayQueue := make([]sync.Map, conf.WorkNums)
	queue = DelayQueue{
		delayQueue: delayQueue,
		conf:       conf,
	}
	return
}

// 运行队列监听goroutine
func (q *DelayQueue) Run() {
	ticker := time.NewTicker(time.Millisecond * 600)
	// 启动Work
	for i := 0; i < q.conf.WorkNums; i++ {

		go func(queueId int) {
			fmt.Printf("queue starting: [%d]\n", queueId)
			for {

				// 到达执行时间，异步执行延时任务
				nowTime := time.Now().Unix()
				q.delayQueue[queueId].Range(func(doTime, jobList any) bool {
					if nowTime >= doTime.(int64) {

						for _, job := range jobList.([]JobData) {
							go q.do(job)
						}
						q.delayQueue[queueId].Delete(doTime)
					}
					return true
				})
				<-ticker.C // 休息一下，避免cpu占用
			}
		}(i)
	}
}

// 到达执行时间，执行异步任务
func (q *DelayQueue) do(job JobData) {
	fmt.Printf("%s job start: %s\n", time.Now().Format("2006-01-02 15:04:05"), job.Id)

	ok := job.Handler(job.Id, job.Data)
	if !ok {
		fmt.Printf("%s job run failed: %s\n", time.Now().Format("2006-01-02 15:04:05"), job.Id)
	}
	fmt.Printf("%s job run succeed: %s\n", time.Now().Format("2006-01-02 15:04:05"), job.Id)
}

// 增加延时队列消费任务
//
// doTime: 执行时间；jobData: json格式的字符串数据
func (q *DelayQueue) AddJob(doTime int64, jobData JobData) {
	// 1.随机获取到其中一个队列
	randQueId := rand.Intn(4)

	// 2.获取对应的任务队列
	jobList := make([]JobData, 0)
	oldJobList, ok := q.delayQueue[randQueId].Load(doTime)
	if ok {
		jobList = oldJobList.([]JobData)
	}

	// 3.加入新任务，并更新原有队列
	jobList = append(jobList, jobData)
	q.delayQueue[randQueId].Swap(doTime, jobList)
	fmt.Printf("add jobData to queue: %d\n", randQueId)
}
