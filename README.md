# delay-queue

#### 介绍
纯golang实现的延时队列

#### 安装教程

1.  需要设置一下私有库连接，不然不会解析到：`go env -w GOPRIVATE=gitee.com`
2.  安装：`go get gitee.com/ted-bug/delay-queue`

#### 使用说明

1.  基本使用方式可以查看测试用例`test/`下的文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request